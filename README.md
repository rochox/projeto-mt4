﻿# Projeto MT4
>Desafio da MT4 tecnologia.

Projeto foi desenvolvido utilizando ferramentas como o __Bootstrap 4, jQuery, Font Awesome, Datatables e a lib phpseclib__ que é utilizada para executar comandos SSH, sua escolha se dá ao fato de ser simples na sua utilização e não ser necessário alguma configuração extra.

## Instalação
Antes de começar a testar a aplicação, você precisa executar a seguinte query no seu servidor MySQL para criar um banco de dados adequado para o projeto:

```
CREATE DATABASE IF NOT EXISTS `MT4`;

USE `MT4`;

CREATE TABLE IF NOT EXISTS `dispositivos` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` varchar(200),
  `ip` varchar(200),
  `tipo`varchar(200),
  `fabricante`varchar(200),
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;
```

Após isso é necessário preencher as informações sobre seu banco de dados no `app/config.php` para que a aplicação de comunique com o MySQL.

```
define('DB_TYPE', 'mysql');
define('DB_HOST', 'O IP DO SERVIDOR PARA CONECTAR');
define('DB_NAME', 'NOME DO BANCO DE DADOS');
define('DB_USER', 'USUARIO DO BANCO DE DADOS');
define('DB_PASSWORD', 'SENHA DO BANCO DE DADOS');
define('DB_CHARSET', 'utf8');
```

Substitua a descrição pelos valores pedidos.

## Sobre a aplicação
A aplicação utiliza como arquitetura o padrão MVC, que foi escolhido devido ao seu ampla utilização no mercado, sendo mais fácil de encontrar referências e informações sobre o seu uso, e também pela minha maior familiaridade com essa arquitetura. O ponto de partida da aplicação será o ``index.php`` que é responsável por definir os diretórios úteis e iniciar a aplicação. Como no projeto não pode ser utilizado o _mod_rewrite_, para obter o controller, ação e parâmetros, vamos utilizar o _GET_, então ao invés de a aplicação obter essas informações com a URL ``controller/acao/parametro`` será usado ``controller?acao?parâmetros``.

É usado 4 controllers neste projeto, sendo eles o Home, Dispositivo, Extras e Error.

- __Home__: Gera somente a view principal da aplicação, que não contém nenhuma informação, apenas o menu.

- __Dispositivo__: É responsável por todas as ações e views relacionadas ao dispositivo, ou seja, a gravação, atualização, exclusão, consulta, execução SSH, tudo isso com o auxílio do Model que executa as querys necessárias diretamente com o banco de dados e com o _phpseclib_ que faz a conexão com o servidor SSH de acordo com os parâmetros enviados pelo usuário

- __Extras__: Contém toda a lógica responsável pela comparação e criação de hashs e criptografia e descriptografia. Com isso, o controller Extras acaba sendo o mais complexo, pois o mesmo faz utilizações apenas de funções internas, algumas obtendo e respondendo a requisição POST e outras de propriedade _protected_ que apenas calcula e retorna valores para uso das funções de propriedade _public_.

- __Error__: Somente gera as views para a exibição do erro 404, caso o ``app.php`` não encontre o controller.

Usamos 1 model no projeto:
- __DispositivoModel__: Obtém, exclui, verifica, e atualiza dados, retornando para poder ser utilizado no controller.



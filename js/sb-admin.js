(function($) {
  "use strict"; // Start of use strict
  // Configure tooltips for collapsed side navigation
  $('.navbar-sidenav [data-toggle="tooltip"]').tooltip({
    template: '<div class="tooltip navbar-sidenav-tooltip" role="tooltip" style="pointer-events: none;"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
  })
  // Toggle the side navigation
  $("#sidenavToggler").click(function(e) {
    e.preventDefault();
    $("body").toggleClass("sidenav-toggled");
    $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
    $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
  });
  // Force the toggled class to be removed when a collapsible nav link is clicked
  $(".navbar-sidenav .nav-link-collapse").click(function(e) {
    e.preventDefault();
    $("body").removeClass("sidenav-toggled");
  });
  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .navbar-sidenav, body.fixed-nav .sidenav-toggler, body.fixed-nav .navbar-collapse').on('mousewheel DOMMouseScroll', function(e) {
    var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;
    this.scrollTop += (delta < 0 ? 1 : -1) * 30;
    e.preventDefault();
  });
  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });
  // Configure tooltips globally
  $('[data-toggle="tooltip"]').tooltip()
  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    event.preventDefault();
  });
})(jQuery); // End of use strict

$('#cmdModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var recipient = button.data('url') 
  var modal = $(this)
  modal.find('.modal-content').load(recipient)
});


      $(document).ready(function(){
          $('#btn_encript').click(function(){
              cifrar($(this));
          })
          $('#btn_decript').click(function(){
              decifrar($(this));
          })
  
          function cifrar(obj)
          {
              
              var string = $('#encriptar').val();
              var chave = $('#txt_key_encript').val();
              $.ajax({
                  type: "POST",
                  url: "?extras?criptografa",
                  data: {'criptografar':1,'encriptar': string, 'chave_enc': chave},
                  dataType: "json",
                  success : function(data){
                      $('#encriptar').val(data);
                  }
              });
          }
          function decifrar(obj)
          {
        
              var string = $('#decriptar').val();
              var chave = $('#txt_key_decript').val();
              
              $.ajax({
                  type: "POST",
                  url: "?extras?descriptografa",
                  data: {'descriptografar':1,'decriptar': string, 'chave_dec': chave},
                  dataType: "json",
                  success : function(data){
                      $('#decriptar').val(data);
                  }
              });
          }
      });

      $(document).ready(function(){
        $('#btn_comparar').click(function(){
            comparar($(this));
        })

        function comparar(obj)
        {
            
            var txt = $('#txt').val();
            var hash = $('#hash').val();
            var key = $('#chave').val();
            $.ajax({
                type: "POST",
                url: "?extras?compara",
                data: {'texto': txt, 'hash': hash, 'chave': key},
                dataType: "json",
                success : function(data){
                    $('#sha512').val(data.sha512.valor);
                    $('#hmac').val(data.hmac.valor);
                    $('#md5').val(data.md5.valor);
                    $('#resultado-md5').html(data.md5.igual);
                    $('#resultado-sha512').html(data.sha512.igual);
                    $('#resultado-hmac').html(data.hmac.igual);
                },
                error : function(e){
                  console.log(e);
                }
            });
        }
    });

 
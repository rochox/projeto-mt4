<?php
define('ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);

define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);

define('VENDOR', ROOT . 'vendor' . DIRECTORY_SEPARATOR);

// Configurações
require APP . 'config.php';

// Classes essênciais
require APP . 'app.php';
require APP . 'controller.php';


// inicia 
$app = new App();


<?php

class Controller
{
    
    public $db = null;
    public $model = null;
    public $nomeModel = "";

 
    function __construct()
    {
        $this->abrirConexao();
        $this->carregarModel($this->nomeModel);
    }

    // abre o bd com as credenciais fornecidas no config.php
    private function abrirConexao()
    {
        $opcao = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);

        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASSWORD, $opcao);
    }

    // carrega o modelo 
    public function carregarModel($n)
    {
        $n = $n."Model";
        require APP . 'model/'.$n.'.php';
        
        $this->model = new $n($this->db);
    }
}

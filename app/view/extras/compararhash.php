<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">
          <a href="?">Dashboard</a>
          <li class="breadcrumb-item active">Criptografar</li>
        </li>
      </ol>
      
      <div class="row">
        <div class="col-12">
        <h1>Compara Hash</h1>
          <p>Aqui você pode comparar hashs!</p>
          <p>SHA512 - HMAC - MD5</p>
        <form class="" action="" method="post">
                
                
                <div class="form-row">
                    <div class="col">
                        <div class="input-group">
                            <input type="text" name="texto" class="form-control" id="txt" placeholder="Insira aqui seu texto" required>
                            
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="form-row">
                    <div class="col">
                        <div class="input-group">
                        <div class="input-group-prepend">
                        <div class="input-group-text">Seu hash:</div>
                            </div>
                            <input type="text" name="hash" class="form-control" id="hash" placeholder="Digite aqui seu hash para comparar, caso esteja vazio, não será comparado">
                           
                        </div>
                        
                    </div>
                  </div>
                  <Br>
                  <div class="form-row">
                    <div class="col">
                        <div class="input-group">
                        <div class="input-group-prepend">
                        <div class="input-group-text">Chave privada:</div>
                            </div>
                            <input type="text" class="form-control" id="chave"  placeholder="Digite aqui sua chave usada para gerar o HMAC SHA512" required>
                            <div class="input-group-prepend">
                                <button type="button" name="comparar" id="btn_comparar" class="btn btn-primary">Comparar / Gerar</button>
                            </div>
                        </div>
                        
                    </div>
                  </div>
                  <br>
                  <fieldset disabled>
                  <div class="form-row">
                    <div class="col">
                        <div class="input-group">
                        <div class="input-group-prepend">
                        <div class="input-group-text success">SHA512: </div>
                            </div>
                            <input type="text" name="texto" class="form-control" id="sha512" disabled>
                            <div class="input-group-prepend">
                            <div class="input-group-text" id="resultado-sha512"></div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <BR>
                  <div class="form-row">
                    <div class="col">
                        <div class="input-group">
                        <div class="input-group-prepend">
                        <div class="input-group-text">HMAC SHA512: </div>
                            </div>
                            <input type="text" name="texto" class="form-control" id="hmac" disabled>
                            <div class="input-group-prepend">
                            <div class="input-group-text" id="resultado-hmac"></div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <br>
                  <div class="form-row">
                    <div class="col">
                        <div class="input-group">
                        <div class="input-group-prepend">
                        <div class="input-group-text">MD5: </div>
                            </div>
                            <input type="text" name="texto" class="form-control" id="md5" disabled>
                            <div class="input-group-prepend">
                            <div class="input-group-text" id="resultado-md5"></div>
                            </div>
                        </div>
                    </div>
                    </fieldset>
                  </div>
            </form>
            
        </div>
      </div>
    </div>
  </div>
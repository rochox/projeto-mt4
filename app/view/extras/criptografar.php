<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">
          <a href="?">Dashboard</a>
          <li class="breadcrumb-item active">Criptografar</li>
        </li>
      </ol>
      
      <div class="row">
        <div class="col-12">
        <h1>Criptografador</h1>
          <p>Aqui você pode codificar e decodificar usando alguns métodos!</p>
          <p>Cifra de césar + AES 256:</p>
        <form class="" action="" method="post">
                <div class="form-row">
                
                    <div class="col">
                        <textarea name="encriptar" id="encriptar" class="form-control" rows="8" cols="80" placeholder="Insira aqui o texto que deseja codificar"></textarea>
                    </div>
                    <div class="col">
                        <textarea name="decriptar" id="decriptar" class="form-control" rows="8" cols="80" placeholder="Insira aqui o texto que deseja decifrar"></textarea>
                    </div>
                </div>
                <br>
                <div class="form-row">
                    <div class="col">
                        <div class="input-group">
                            <input type="number" name="chave_enc" class="form-control" id="txt_key_encript" placeholder="Insira aqui sua chave">
                            <div class="input-group-prepend">
                                <button type="button" name="criptografar_cesar" id="btn_encript" class="btn btn-primary">Codificar</button>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group">
                            <input type="number" name="chave_dec" class="form-control" id="txt_key_decript" placeholder="Insira aqui sua chave">
                            <div class="input-group-prepend">
                                <button type="button" name="descriptografar_cesar" id="btn_decript" class="btn btn-success">Decodificar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
        </div>
      </div>
    </div>
  </div>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Cadastrar</li>
      </ol>
      <div class="row">
        <div class="col-12">
          <h1>Cadastrando novo dispositivo</h1>
          <form action="?dispositivo?adicionar" method="POST">

  <div class="form-group row">
    <label for="hostname" class="col-3 col-form-label">Hostname</label> 
    <div class="col-9">
    <input id="hostname" name="hostname" type="text"  class="form-control here">
    </div>
  </div>
  <div class="form-group row">
    <label for="ip" class="col-3 col-form-label">IP</label> 
    <div class="col-9">
      <div class="input-group">
        <div class="input-group-text"><div class="input-group-prepend">
          <i class="fa fa-globe"></i>
        </div> </div>
        <input id="ip" name="ip" type="text" class="form-control here">
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="tipo" class="col-3 col-form-label">Tipo</label> 
    <div class="col-9">
    <input id="tipo" name="tipo" type="text" class="form-control here">
      <!-- <select id="tipo" name="tipo" required="required" class="custom-select">
        <option value="Servidor">Servidor</option>
        <option value="Roteador">Roteador</option>
        <option value="Switch">Switch</option>
      </select> -->
    </div>
  </div>
  <div class="form-group row">
    <label for="fabricante" class="col-3 col-form-label">Fabricante</label> 
    <div class="col-9">
      <div class="input-group">
      <div class="input-group-text"><div class="input-group-prepend">
          <i class="fa fa-industry"></i>
        </div> </div>
        <input id="fabricante" name="fabricante" type="text"  class="form-control here">
      </div>
    </div>
  </div> 
  <div class="form-group row">
    <div class="offset-3 col-9">
      <button name="cadastrar_dispositivo" type="submit" class="btn btn-primary">Salvar</button>
    </div>
  </div>
</form>
        </div>
      </div>
    </div>
  </div>
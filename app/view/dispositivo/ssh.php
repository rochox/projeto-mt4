<!-- modal header -->
<div class="modal-header">
        <h5 class="modal-title" id="viewModal">Conexão SSH</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- modal body -->
<div class="modal-body">
  <div class="form-group row">
    <label for="ip" class="col-3 col-form-label">IP</label> 
    <div class="col-9">
      <div class="input-group">
        <div class="input-group-text"><div class="input-group-prepend">
          <i class="fa fa-globe"></i>
        </div> </div>
        <input id="ip" name="ip" value="<?php echo htmlspecialchars($disp->ip, ENT_QUOTES, 'UTF-8'); ?>" type="text" class="form-control here">
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="tipo" class="col-3 col-form-label">Usuario</label> 
    <div class="col-9">
    <input id="usuario" name="Usuario" type="text" class="form-control here">
    </div>
  </div>
  <div class="form-group row">
    <label for="tipo" class="col-3 col-form-label">Senha</label> 
    <div class="col-9">
    <input id="senha" name="senha" type="text" class="form-control here">
    </div>
  </div>
  <div class="form-group row">
    <label for="tipo" class="col-3 col-form-label">Comando SSH</label> 
    <div class="col-9">
    <input id="comando" name="comando" type="text" class="form-control here">
    </div>
  </div>
  <div class="form-group row">
    <div class="offset-3 col-9">
      <button type="button"  id="btn_executar" class="btn btn-primary">Enviar</button>
    </div>
  </div>
  <div class="form-group row">
  <label for="tipo" class="col-3 col-form-label">Resposta</label> 
<div class="col-9">
<textarea class="form-control" type="text" id="resposta"  readonly></textarea>
    </div>
  </div>
</div>
<script>
   $(document).ready(function(){
      $('#btn_executar').click(function(){
          executar($(this));
      })

      function executar(obj)
      {
          
          var ip = $('#ip').val();
          var usuario = $('#usuario').val();
          var senha = $('#senha').val();
          var comando = $('#comando').val();
          $.ajax({
              type: "POST",
              url: "?dispositivo?executar",
              data: {'ip': ip, 'usuario': usuario, 'senha': senha, 'comando': comando},
              
              success : function(data){
                  console.log(data);
                  $('#resposta').val(data);
              },
              error : function(e){
                $('#resposta').val("Erro");
                console.log(e);
              }
          });
      }
  });</script>
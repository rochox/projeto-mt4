<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Listar</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Listando cadastrados</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Hostname</th>
                  <th>IP</th>
                  <th>Tipo</th>
                  <th>Fabricante</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                <th>ID</th>
                  <th>Hostname</th>
                  <th>IP</th>
                  <th>Tipo</th>
                  <th>Fabricante</th>
                  <th>Ações</th>
                </tr>
              </tfoot>
              <tbody>
              <?php foreach ($dispositivos as $disp) { ?>
                <tr>
                  <td><?php if (isset($disp->id)) echo htmlspecialchars($disp->id, ENT_QUOTES, 'UTF-8'); ?></td>
                  <td><?php if (isset($disp->hostname)) echo htmlspecialchars($disp->hostname, ENT_QUOTES, 'UTF-8'); ?></td>
                  <td><?php if (isset($disp->ip)) echo htmlspecialchars($disp->ip, ENT_QUOTES, 'UTF-8'); ?></td>
                  <td><?php if (isset($disp->tipo)) echo htmlspecialchars($disp->tipo, ENT_QUOTES, 'UTF-8'); ?></td>
                  <td><?php if (isset($disp->fabricante)) echo htmlspecialchars($disp->fabricante, ENT_QUOTES, 'UTF-8'); ?></td>
                  <td class="actions">
                    <div class="btn-group" role="group">                
               <a href="<?php if (isset($disp->id)) echo '?dispositivo?editar?'.htmlspecialchars($disp->id, ENT_QUOTES, 'UTF-8'); ?>"> <button type="button" class="btn btn-light" name="edit"  ><i class="fa fa-edit" ></i></button></a>
              <button type="button" class="btn btn-dark" name="ssh" data-toggle="modal" data-url="<?php if (isset($disp->id)) echo '?dispositivo?conectar?'.htmlspecialchars($disp->id, ENT_QUOTES, 'UTF-8'); ?>" data-target="#cmdModal" ><i class="fas fa-terminal" ></i></button>
              <a href="<?php if (isset($disp->id)) echo '?dispositivo?excluir?'.htmlspecialchars($disp->id, ENT_QUOTES, 'UTF-8'); ?> "> <button type="button" class="btn btn-danger"><i class="fa fa-trash-alt"></i></button></a>
              </div>
            </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>
        <div class="card-footer small text-muted">Atualizado em: <?php echo date("d-m-Y H:i:s"); ?></div>
      </div>
    </div>
  </div>
              </div>
              <!-- SSH Modal -->
<div class="modal fade bd-example-modal-lg" id="cmdModal" tabindex="-1" role="dialog" aria-labelledby="cmdModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

    </div>
  </div>
</div>
  <!-- End ssh Modal -->


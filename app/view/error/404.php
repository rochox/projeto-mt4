
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Erro 404</li>
      </ol>
      <div class="row">
        <div class="col-12">
        <h1 class="display-1">404</h1>
  <p class="lead">Página não encontrada. Você pode
    <a href="javascript:history.back()">voltar</a>
    para a pagina anterior, ou
    <a href="?">voltar ao início</a>.</p>

        </div>
      </div>
    </div>
  </div>


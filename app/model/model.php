<?php
// Classe genérica para estabelecer conexão com o banco de dados
class Model
{
    
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('A conexão com o banco de dados não pôde ser realizada.');
        }
    }

}

<?php 

    // Model de dispositivo
   require('model.php');

    Class DispositivoModel extends Model{

        // obtém dispositivos para listagem
    public function getDisps()
    {
        $sql = "SELECT id, hostname, ip, tipo, fabricante FROM dispositivo";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    // adiciona dispositivos a base
    public function addDisp($hostname, $ip, $tipo, $fabricante)
    {
        $sql = "INSERT INTO dispositivo (hostname, ip, tipo, fabricante) VALUES (:hostname, :ip, :tipo, :fabricante)";
        $query = $this->db->prepare($sql);
        $parameters = array(':hostname' => $hostname, ':ip' => $ip, ':tipo' => $tipo, ':fabricante' => $fabricante);

        $query->execute($parameters);
    }

    // deletando um dispositivo
    public function deleteDisp($disp_id)
    {
        $sql = "DELETE FROM dispositivo WHERE id = :dispositivo_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':dispositivo_id' => $disp_id);

        $query->execute($parameters);
    }

    //checando se o dispositivo existe mesmo
    public function dispExists($disp_id)
    {
        $sql = "SELECT COUNT(id) AS dispQnt FROM dispositivo WHERE id = :dispositivo_id";
        $query = $this->db->prepare($sql);

        $parameters = array(':dispositivo_id' => $disp_id);

        $query->execute($parameters);
    
        return $query->fetch()->dispQnt == 1 ? true : false;

    }

    // obtendo um dispositivo
    public function getDisp($disp_id)
    {
        $sql = "SELECT id, hostname, ip, tipo, fabricante FROM dispositivo WHERE id = :dispositivo_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':dispositivo_id' => $disp_id);

        $query->execute($parameters);

       
        return $query->fetch();
    }

    public function getDispIp($disp_id)
    {
        $sql = "SELECT ip FROM dispositivo WHERE id = :dispositivo_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':dispositivo_id' => $disp_id);

        $query->execute($parameters);

       
        return $query->fetch();
    }

    // atualizando dispositivo

    public function updateDisp($disp_id, $hostname, $ip, $tipo, $fabricante)
    {
        $sql = "UPDATE dispositivo SET (hostname, ip, tipo, fabricante) VALUES (:hostname, :ip, :tipo, :fabricante) WHERE id = :disp_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':hostname' => $hostname, ':ip' => $ip, ':tipo' => $tipo, ':fabricante' => $fabricante ,':disp_id' => $disp_id);

        $query->execute($parameters);
    }

    }
    ?>
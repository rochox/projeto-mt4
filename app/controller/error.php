<?php


class Error extends Controller
{
 
    public function index()
    {
        
        require APP . 'view/template/header.php';
        require APP . 'view/error/index.php';
        require APP . 'view/template/footer.php';
    }

    public function notfound()
    {
       
        require APP . 'view/template/header.php';
        require APP . 'view/error/404.php';
        require APP . 'view/template/footer.php';
    }
}

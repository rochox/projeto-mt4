<?php

class Home extends Controller
{
    
    public function index()
    {
        // carrega as views
        require APP . 'view/template/header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/template/footer.php';
    }
}

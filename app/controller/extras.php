<?php

class Extras extends Controller
{

    public function index()
    {
        $this->viewCriptografar();
    }

    public function hash()
    {
        $this->viewComparaHash();
    }


    public function viewCriptografar()
    {

        require APP . 'view/template/header.php';
        require APP . 'view/extras/criptografar.php';
        require APP . 'view/template/footer.php';
    }

    public function viewComparaHash()
    {

        require APP . 'view/template/header.php';
        require APP . 'view/extras/compararhash.php';
        require APP . 'view/template/footer.php';
    }


    public function compara()
    {

        if (!empty($_POST["texto"])) {

            $str = $_POST["texto"];
            $resultado = array();
            $chave;
            $hash;

            if (empty($_POST["chave"])) {
                $chave = 'secret';
            } else {
                $chave = $_POST["chave"];
            }

            $resultado['sha512']['valor'] = hash('sha512', $str);
            $resultado['sha512']['igual'] = "";
            $resultado['md5']['valor'] = md5($str);
            $resultado['md5']['igual'] = "";
            $resultado['hmac']['valor'] = hash_hmac('sha512', $str, $chave);
            $resultado['hmac']['igual'] = "";


            if (!empty($_POST["hash"])) {
                $hash = $_POST["hash"];
                $resultado['sha512']['igual'] = "Diferente";
                $resultado['md5']['igual'] = "Diferente";
                $resultado['hmac']['igual'] = "Diferente";
                switch ($hash) {
                    case $resultado['sha512']['valor']:
                        $resultado['sha512']['igual'] = "Igual";
                        break;
                    case $resultado['md5']['valor']:
                        $resultado['md5']['igual'] = "Igual";
                        break;
                    case $resultado['hmac']['valor']:
                        $resultado['hmac']['igual'] = "Igual";
                        break;
                }
            }


            echo json_encode($resultado);
        }

    }



    public function criptografa()
    {

        if (isset($_POST["criptografar"])) {

            $texto = $this->encriptar($_POST["encriptar"], $_POST["chave_enc"]);
            echo json_encode($texto);
        }

    }
    public function descriptografa()
    {

        if (isset($_POST["descriptografar"])) {

            $texto = $this->decriptar($_POST["decriptar"], $_POST["chave_dec"]);
            echo json_encode($texto);

        }

    }

    protected function encriptar($txt, $chave)
    {
        $cifra = implode('', array_map(function ($caracter) use ($chave) {
            return $this->embaralha($caracter, $chave);
        }, str_split($txt)));

        $aesEncriptografar = $this->aesEncrypt($cifra);

        return $aesEncriptografar;
    }

    protected function decriptar($txt, $chave)
    {
        $chave *= -1;

        $aesDescriptografar = $this->aesDecrypt($txt);

        $decifra = implode('', array_map(function ($caracter) use ($chave) {
            return $this->embaralha($caracter, $chave);
        }, str_split($aesDescriptografar)));

        return $decifra;
    }

    protected function embaralha($caracter, $embaralhar)
    {
        $embaralhar = $embaralhar % 25;
        $ascii = ord($caracter);
        $embaralhado = $ascii + $embaralhar;
        if ($ascii >= 65 && $ascii <= 90) {
            return chr($this->wrapUppercase($embaralhado));
        }
        if ($ascii >= 97 && $ascii <= 122) {
            return chr($this->wrapLowercase($embaralhado));
        }
        return chr($ascii);
    }

    protected function wrapUppercase($ascii)
    {
        if ($ascii < 65) {
            $ascii = 91 - (65 - $ascii);
        }

        if ($ascii > 90) {
            $ascii = ($ascii - 90) + 64;
        }
        return $ascii;
    }

    protected function wrapLowercase($ascii)
    {
        if ($ascii < 97) {
            $ascii = 123 - (97 - $ascii);
        }

        if ($ascii > 122) {
            $ascii = ($ascii - 122) + 96;
        }
        return $ascii;
    }

    protected function aesEncrypt($str)
    {
        $chave = pack('H*', "bcb04b7e103a0cd8b54763151cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
        return rtrim(
            base64_encode(
                mcrypt_encrypt(
                    MCRYPT_RIJNDAEL_256,
                    $chave,
                    $str,
                    MCRYPT_MODE_ECB,
                    mcrypt_create_iv(
                        mcrypt_get_iv_size(
                            MCRYPT_RIJNDAEL_256,
                            MCRYPT_MODE_ECB
                        ),
                        MCRYPT_RAND
                    )
                )
            ),
            "\0"
        );
    }

    protected function aesDecrypt($str)
    {
        $chave = pack('H*', "bcb04b7e103a0cd8b54763151cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_256,
                $chave,
                base64_decode($str),
                MCRYPT_MODE_ECB,
                mcrypt_create_iv(
                    mcrypt_get_iv_size(
                        MCRYPT_RIJNDAEL_256,
                        MCRYPT_MODE_ECB
                    ),
                    MCRYPT_RAND
                )
            ),
            "\0"
        );
    }


}



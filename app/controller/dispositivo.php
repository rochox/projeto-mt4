<?php

class Dispositivo extends Controller
{
    public $nomeModel = "dispositivo"; // nome do model que vamos utilizar

    public function index()
    {

        $dispositivos = $this->model->getDisps(); // pega os dispositivos
        // carrega as views
        require APP . 'view/template/header.php';
        require APP . 'view/dispositivo/index.php';
        require APP . 'view/template/footer.php';
    }
    public function cadastrar()
    {
        // carrega as views
        require APP . 'view/template/header.php';
        require APP . 'view/dispositivo/cadastrar.php';
        require APP . 'view/template/footer.php';
    }

    public function ssh()
    {

    }

    public function listar()
    {
        $this->index();
    }

    public function editar($disp_id)
    {

        if (isset($disp_id)) {
            if ($this->model->dispExists($disp_id)) {
                $disp = $this->model->getDisp($disp_id);
                require APP . 'view/template/header.php';
                require APP . 'view/dispositivo/editar.php';
                require APP . 'view/template/footer.php';
                echo "true";
            } else {
                header('location: ?dispositivo');
            }
        } else {
            header('location: ?dispositivo');
        }
    }

    public function adicionar()
    {

        if (isset($_POST["cadastrar_dispositivo"])) {

            $this->model->addDisp($_POST["hostname"], $_POST["ip"], $_POST["tipo"], $_POST["fabricante"]);
        }


        header('location: ?dispositivo');
    }

    public function atualizar()
    {

        if (isset($_POST["atualizar_dispositivos"])) {

            $this->model->updateDisp($_POST["id"], $_POST["hostname"], $_POST["ip"], $_POST["tipo"], $_POST['fabricante']);
        }


        header('location: ?dispositivo');
    }

    public function excluir($disp_id)
    {

        if (isset($disp_id)) {
            if ($this->model->dispExists($disp_id)) {
                $this->model->deleteDisp($disp_id);
            }
        }
        header('location: ?dispositivo');
    }

    public function conectar($disp_id)
    {

        if (isset($disp_id)) {

            if ($this->model->dispExists($disp_id)) {
                $disp = $this->model->getDispIp($disp_id);


                require APP . 'view/dispositivo/ssh.php';


            }
        }

    }
    public function executar()
    {

        if (empty($_POST["ip"]) and empty($_POST["usuario"]) and empty($_POST["senha"]) and empty($_POST["comando"])) {
            echo "Preenche todos os campos";
                }else{
            error_reporting(0);
            set_include_path(VENDOR . 'phpseclib');
            include('Net/SSH2.php');

            $ssh = new Net_SSH2($_POST["ip"]);

            
            if (!$ssh->login($_POST["usuario"], $_POST["senha"])) {
                exit('Falha no login!');
            }

            echo $ssh->exec($_POST["comando"]);
            

        }


    }
}

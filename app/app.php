<?php

class App
{
  
    private $url_controlador = null;
   
    private $url_acao = null;
 
    private $url_parametros = array();

   
    public function __construct()
    {
        
        $this->processaUrl();

        // Checar se há algum controlador definido na url
        if (!$this->url_controlador) {

            require APP . 'controller/home.php';
            $page = new Home();
            $page->index();

        } 
        // checa se o controlador existe
        elseif (file_exists(APP . 'controller/' . $this->url_controlador . '.php')) {
            
            // se existe, entao abrimos e inicializamos
            require APP . 'controller/' . $this->url_controlador . '.php';
            $this->url_controlador = new $this->url_controlador();

            // checa se o metodo existe
            if (method_exists($this->url_controlador, $this->url_acao)) {

                if (!empty($this->url_parametros)) {
                    // se há parametros, chamamos o metodo e passamos eles
                    call_user_func_array(array($this->url_controlador, $this->url_acao), $this->url_parametros);
                } else {
                    // se não temos parametros chamamos o metodo sem parametros
                    $this->url_controlador->{$this->url_acao}();
                }

            } else {
                if (strlen($this->url_acao) == 0) {
                    // se nenhum metodo for definido, entao chamamos o padrão, o metodo index
                    $this->url_controlador->index();
                }
                else {
                    header('location: ?error?notfound');
                }
            }
        } else {
            header('location: ?error?notfound');
        }
    }

   
    private function processaUrl()
    {
        // como não deve ser utilizado mod_rewrite no projeto, é feito o seguinte:
        if (!empty($_GET)) { 
            // verificamos e puxamos os parametros atraves do GET
            $url = array_keys($_GET);
            $url = trim($url[0], '?');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('?', $url);

            // Coloca partes da url de acordo com suas propriedades
            $this->url_controlador = isset($url[0]) ? $url[0] : null;
            $this->url_acao = isset($url[1]) ? $url[1] : null;

            // remove as propriedades do controlador e do action da variavel
            unset($url[0], $url[1]);

            $this->url_parametros = array_values($url);

          
          
        }
    }
}
